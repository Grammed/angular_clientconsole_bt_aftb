import {Injectable} from '@angular/core';
import {CanActivate} from '@angular/router';
import {Router} from '@angular/router';
import {AngularFireAuth} from '@angular/fire/auth';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

@Injectable()

export class AuthGuard implements CanActivate {
  constructor(
   private router: Router,
   private afAuth: AngularFireAuth,
    ) {}

// Again encapsulating .pipe around an especially imported 'map'
  canActivate(): Observable<boolean> {
    return this.afAuth.authState.pipe(
      map(auth => {
      if (!auth) {
        this.router.navigate(['/login']);
        return false;
      } else {
        return true;
      }
    }));
  }



}
