import {Injectable} from '@angular/core';
import {Settings} from '../models/Settings';
import {stringify} from '@angular/compiler/src/util';

@Injectable()
export class SettingsService {

  settings: Settings = {
    allowRegistration: true,
    disableBalanceOnAdd: true,
    disableBalanceOnEdit: true};

  constructor() {
    if (localStorage.getItem('settings') != null) {
      this.settings = JSON.parse(localStorage.getItem('settings'));
    }
  }

  getSettings(): Settings {
    return this.settings;
  }

  setSettings(settings: Settings): void {
    localStorage.setItem('settings', JSON.stringify(settings));
  }

}
