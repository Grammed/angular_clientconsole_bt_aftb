import { Injectable } from '@angular/core';
import { AngularFireAuth} from '@angular/fire/auth';
import { Observable } from 'rxjs';
import firebase from 'firebase';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private afAuth: AngularFireAuth) { }

  // https://stackoverflow.com/questions/60455433/property-auth-does-not-exist-on-type-angularfireauth
  login(email: string, password: string): Promise<unknown> {
    return new Promise((resolve, reject) => {
      this.afAuth.signInWithEmailAndPassword(email, password)
        .then(userData => resolve(userData),
            err => reject(err));
    });
  }

  register(email: string, password: string): Promise<unknown> {
    return new Promise((resolve, reject) => {
      this.afAuth.createUserWithEmailAndPassword(email, password)
        .then(userData => resolve(userData),
          err => reject(err));
    });
  }

  // See firebase docs for this.
  getAuth(): Observable<firebase.User> {
    // set auth to auth (observed state of authorisation
    return this.afAuth.authState.pipe(authState => authState);
  }

  logout(): void {
    this.afAuth.signOut();
  }

}
