import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { FlashMessagesService} from 'angular2-flash-messages';
import { Router} from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styles: [
  ]
})
export class LoginComponent implements OnInit {
  email: string;
  password: string;

  constructor(
    private authService: AuthService,
    private router: Router,
    private flashMessage: FlashMessagesService) { }

  ngOnInit(): void {
    // if authorised === true you are logged in, login page now unavailable.
    this.authService.getAuth().subscribe(auth => {
     if (auth) {
       this.router.navigate(['/']);
     }
    });
  }

  // When you are grabbing on to a promise return you have to use .then
  onSubmit(): void {
    this.authService.login(this.email, this.password)
      .then(response => {
        this.flashMessage.show('You are now logged in', {
          cssClass: 'alert-success', timeout: 4000
        });
        this.router.navigate(['/']);
      })
      .catch(error => {
        this.flashMessage.show(error.message, {
          cssClass: 'alert-danger', timeout: 4000
        });
      });
  }


}
