import {Component, OnInit} from '@angular/core';
import {ClientService} from '../../services/client.service';
import {FlashMessagesService} from 'angular2-flash-messages';
import {ActivatedRoute, Router} from '@angular/router';
import {Client} from '../../models/Client';
import {SettingsService} from '../../services/settings.service';


@Component({
  selector: 'app-edit-client',
  templateUrl: './edit-client.component.html',
  styles: []
})
export class EditClientComponent implements OnInit {
  id: string;
  client: Client = {
    firstName: '',
    lastName: '',
    email: '',
    phone: '',
    balance: 0
  };

  // eventually found in settings
  disableBalanceOnEdit;


  constructor(
    private settingsService: SettingsService,
    private clientService: ClientService,
    private flashMessage: FlashMessagesService,
    private route: ActivatedRoute,
    private router: Router) {
  }

  ngOnInit(): void {
    this.disableBalanceOnEdit = this.settingsService.getSettings().disableBalanceOnEdit;
    // Get id from url as soon a page loads id is passed and set from here.
    this.id = this.route.snapshot.params.id;
    this.clientService.getClient(this.id).subscribe(client =>
      this.client = client);
  }

  onSubmit({value, valid}: { value: Client, valid: boolean }): void {
    if (!valid) {
      this.flashMessage.show('Please fill out the form correctly', {
        cssClass: 'alert-danger', timeout: 4000
      });
    } else {
      // problem here is that we need to provide id in order to update, form does not have it
      value.id = this.id;
      // update client
      this.clientService.updateClient(value);
      this.flashMessage.show('Client has been updated successfully', {
        cssClass: 'alert-success', timeout: 4000
      });
      this.router.navigate(['/client/' + this.id]);

    }
  }
}
