import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../services/auth.service';
import {Router} from '@angular/router';
import {FlashMessagesService} from 'angular2-flash-messages';
import {SettingsService} from '../../services/settings.service';


@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
isLoggedIn: boolean;
loggedInUser: string;
showRegister;

  constructor(
    private settingsService: SettingsService,
    private authService: AuthService,
    private router: Router,
    private flashMessage: FlashMessagesService,
  ) {
  }

  ngOnInit(): void {
    this.showRegister = this.settingsService.getSettings().allowRegistration;
    this.authService.getAuth().subscribe(auth => {
      if (auth) {
        this.isLoggedIn = true;
        this.loggedInUser = auth.email;
      }else {
        this.isLoggedIn = false;
      }
    });
  }

  onLogOutClick(): void {
    this.authService.logout();
    this.flashMessage.show('Logout succesful', {cssClass : 'alert-success timeout=4000'});
    this.router.navigate(['/login']);
  }

}
