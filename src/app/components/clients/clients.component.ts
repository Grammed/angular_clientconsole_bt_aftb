import { Component, OnInit } from '@angular/core';
import { ClientService } from '../../services/client.service';
import { Client} from '../../models/Client';

@Component({
  selector: 'app-clients',
  templateUrl: './clients.component.html',
  styles: [
  ]
})
export class ClientsComponent implements OnInit {
  clients: Client[];
  totalOwed: number;

  constructor(private clientService: ClientService) {
  }

  ngOnInit(): void {
    this.clientService.getClients().subscribe(clients =>
    { console.log(clients);
      this.clients = clients;
      this.getTotalOwed();
    });
  }

  // Total was incorrect because client balance was being treated as string, but on parse insists on being toStringed!
  getTotalOwed(): number {
    this.totalOwed = this.clients.reduce((total, client) => {
      return total + parseFloat(client.balance.toString());
    }, 0);
    return null;
  }



}
