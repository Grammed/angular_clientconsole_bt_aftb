import {Component, OnInit} from '@angular/core';
import {ClientService} from '../../services/client.service';
import {FlashMessagesService} from 'angular2-flash-messages';
import {Router, ActivatedRoute, Params} from '@angular/router';
import {Client} from '../../models/Client';


@Component({
  selector: 'app-client-details',
  templateUrl: './client-details.component.html',
  styles: []
})

export class ClientDetailsComponent implements OnInit {
  id: string;
  client: Client;
  /*Gives option for colour coding if no remaining balance*/
  hasBalance = false;
  // Icon click shows form to update balance.
  showBalanceUpdateInput = false;

  constructor(private clientService: ClientService,
              private flashMessage: FlashMessagesService,
              private route: ActivatedRoute,
              private router: Router) {
  }

  ngOnInit(): void {
    // Get id from url as soon a page loads
    this.id = this.route.snapshot.params.id;
    this.clientService.getClient(this.id).subscribe(client => {
      if (client != null) {
        if (client.balance > 0) {
          this.hasBalance = true;
        }
      }
      this.client = client;
    });
  }

  onDeleteClick(): void  {
    if (confirm('Are you sure?')) {
      this.clientService.deleteClient(this.client);
      this.flashMessage.show('Client record deleted', {
        cssClass: 'alert-success', timeout: 4000
      });
      this.router.navigate(['/']);
    }
  }

  updateBalance(): void {
    this.clientService.updateClient(this.client);
    this.flashMessage.show('Balance updated', {
      cssClass: 'alert-success', timeout: 4000
    });
  }

}
