// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
    firebase : {
    apiKey: 'AIzaSyCDYIaU0Z5ORqebCcrlYOI6XgQkxkpnO9g',
    authDomain: 'clientpanelprod-c56da.firebaseapp.com',
    projectId: 'clientpanelprod-c56da',
    storageBucket: 'clientpanelprod-c56da.appspot.com',
    messagingSenderId: '519814420059',
    appId: '1:519814420059:web:119834e3a436451bfa917e',
    measurementId: 'G-2SZKEQL387'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
